import Graph;
import utest.Assert;

@:publicFields
class TestFreightTree {

    function new() {
    }

    function testPathSummaryAbstract() {
        var ps1:PathSummary = [{ mode : 1, dist : 10. }];
        Assert.same([{ mode : 1, dist : 10. }], ps1);
        var ps2 = ps1 + new Arc(0, 1, 5., 0);
        Assert.same([{ mode : 1, dist : 10. }], ps1);
        Assert.same([{ mode : 1, dist : 15. }], ps2);
        var ps3 = ps2 + new Arc(0, 2, 3., 0);
        Assert.same([{ mode : 1, dist : 10. }], ps1);
        Assert.same([{ mode : 1, dist : 15. }], ps2);
        Assert.same([{ mode : 1, dist : 15. }, { mode : 2, dist : 3. }], ps3);
    }

}

