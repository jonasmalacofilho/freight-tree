import Graph;

class TestAll {

    static function main() {
        var testRunner = new utest.Runner();

        testRunner.addCase(new TestFreightTree());
        utest.ui.Report.create(testRunner);

        testRunner.run();
    }

}

