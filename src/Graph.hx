class Graph {
}

@:publicFields
class Vertex {

    // intrinsic properties
    var id:Int;

    // freight tree state
    var cost:Float;
    var parent:Null<Arc>;
    var summary:PathSummary;

}

@:publicFields
class Arc {

    // intrinsic properties
    var id:Int;
    var mode:Mode;
    var dist:Float;
    var cost:Float;  // generalized cost

    function new(id, mode, dist, cost) {
        this.id = id;
        this.mode = mode;
        this.dist = dist;
        this.cost = cost;
    }

}

@:forward(iterator)
abstract PathSummary(Array<PathPart>) from Array<PathPart> {

    var last(get, set):PathPart;

    @:op(A + B) @:commutative
    function addArc(arc:Arc):PathSummary {
        if (this.length == 0 || last.mode != arc.mode) {
            return append({ mode : arc.mode, dist : arc.dist });
        }
        else {
            var cp:PathSummary = this.copy();
            cp.last = { mode : arc.mode, dist : (cp.last.dist + arc.dist) };
            return cp;
        }
    }

    function append(p) {
        return this.concat([p]);
    }

    inline
    function get_last() {
        return this[this.length - 1];
    }

    inline
    function set_last(v) {
        return this[this.length - 1] = v;
    }

}

typedef PathPart = {
    var mode:Mode;
    var dist:Float;
}

typedef Mode = Int;

